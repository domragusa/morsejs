class Telegraph{
    constructor(){
        this.audio = new AudioContext();
        this.snd = this.audio.createOscillator();
        this.amp = this.audio.createGain();
        this.snd.frequency.value = 750;
        this.snd.connect(this.amp);
        this.amp.gain.value = 0;
        this.amp.connect(this.audio.destination);
        this.snd.start();
    }
    play(arr, duration){
        var time = this.audio.currentTime;
        if(!duration)duration=0.08; // duration of a single value
        
        // WORKAROUND:
        // the transition between gain A and B is too slow, it starts immediately
        // after a value is set to arrive smoothly at B.
        // To make the curve steeper it's enough to create an curve with the same
        // total duration but a supersampled set of values so that the transition
        // happens in (duration of a single value/number of copies)
        var b=[];
        arr.forEach(x => b.push.apply(b, [x, x, x, x, x, x, x, x, x, x]));
        
        var tmp = new Float32Array(b);
        this.amp.gain.setValueCurveAtTime(tmp, time+0.5, duration*arr.length);
    }
}
