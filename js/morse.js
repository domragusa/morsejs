class Morse{
    static _invTable(mrsCode){
        for(var i in Morse._table){
            if(Morse._table[i] == mrsCode)
                return i;
        }
        
        return undefined;
    }
/* an implementation of _invTable() made for a tweet (when they were 140 chars)
function(m){var o=1,i;for(i in m){o+=o+(m[i]=='.'?1:0)}
return ' temnaiogkdwrus  qzycxbjp l fvh09 8   7       61       2   3 45'[o-1]}
*/
    static toMorse(str){
        var str = str.toLowerCase(), res = "", tmp;
        for(var i=0; i<str.length; i++){
            if(str[i] == " "){
                res += "/";
            }else{
                tmp = Morse._table[str[i]];
                if(!tmp) continue;
                res += tmp + " ";
            }
        }
        return res;
    }
    static fromMorse(mrs){
        var words = mrs.split("/"), res = "", tmp;
        
        for(var i=0; i<words.length; i++){
            words[i] = words[i].split(" ");
            
            for(var j=0; j<words[i].length; j++){
                tmp = Morse._invTable(words[i][j]);
                if(!tmp) continue;
                res += tmp;
            }
            res += " ";
        }
        return res;
    }
/*
        type                duration
    dot                 (0.133s)
    dash                3*[dot]
    between dot/dash    [dot]
    between letters     3*[dot]
    between words       7*[dot]
   
 */
    static toTimeTable(mrs){
        var buf = [], tmp, last,
        arr = {".": [1, 0], "-": [1, 1, 1, 0], " ": [0, 0], "/": [0, 0, 0, 0, 0, 0]};
        
        for(var i=0; i<mrs.length; i++){
            tmp = arr[mrs[i]];
            if(!tmp) continue;
            
            buf.push.apply(buf, tmp);
        }
        return buf;
    }
}

Morse._table = {
    a: ".-",    b: "-...",  c: "-.-.",  d: "-..",   e: ".",
    f: "..-.",  g: "--.",   h: "....",  i: "..",    j: ".---",
    k: "-.-",   l: ".-..",  m: "--",    n: "-.",    o: "---",
    p: ".--.",  q: "--.-",  r: ".-.",   s: "...",   t: "-",
    u: "..-",   v: "...-",  w: ".--",   x: "-..-",  y: "-.--",
    z: "--..",
    0: "-----", 1: ".----", 2: "..---", 3: "...--", 4: "....-",
    5: ".....", 6: "-....", 7: "--...", 8: "---..", 9: "----."
};
